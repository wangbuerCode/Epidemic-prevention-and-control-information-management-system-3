package com.sanley.coronavirus.entity;/*
Created by shkstart on 2020/2/22.
*/

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
@NoArgsConstructor
@Data
@Accessors(chain=true)
public class Inspect implements Serializable {
    private int testId;
    private int baseId;
    private Date testDate;
    private String ctTest;
    private String nuTest;

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getBaseId() {
        return baseId;
    }

    public void setBaseId(int baseId) {
        this.baseId = baseId;
    }

    public Date getTestDate() {
        return testDate;
    }

    public void setTestDate(Date testDate) {
        this.testDate = testDate;
    }

    public String getCtTest() {
        return ctTest;
    }

    public void setCtTest(String ctTest) {
        this.ctTest = ctTest;
    }

    public String getNuTest() {
        return nuTest;
    }

    public void setNuTest(String nuTest) {
        this.nuTest = nuTest;
    }
}
