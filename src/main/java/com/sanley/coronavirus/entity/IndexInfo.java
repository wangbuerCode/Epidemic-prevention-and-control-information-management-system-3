package com.sanley.coronavirus.entity;/*
Created by shkstart on 2020/3/13.
*/

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@NoArgsConstructor
@Data
@Accessors(chain=true)
public class IndexInfo {
    private int currentPatientNumber;
    private int sumPatientNumber;
    private int deadNumber;
    private double deadRate;
    private int cureNumber;
    private double cureRate;
    private int sumTouchNumber;
    private int currentTouchNumber;
    private List dates;
    private List patientNums;
    private List cureNums;
    private String username;


    public int getCurrentPatientNumber() {
        return currentPatientNumber;
    }

    public void setCurrentPatientNumber(int currentPatientNumber) {
        this.currentPatientNumber = currentPatientNumber;
    }

    public int getSumPatientNumber() {
        return sumPatientNumber;
    }

    public void setSumPatientNumber(int sumPatientNumber) {
        this.sumPatientNumber = sumPatientNumber;
    }

    public int getDeadNumber() {
        return deadNumber;
    }

    public void setDeadNumber(int deadNumber) {
        this.deadNumber = deadNumber;
    }

    public double getDeadRate() {
        return deadRate;
    }

    public void setDeadRate(double deadRate) {
        this.deadRate = deadRate;
    }

    public int getCureNumber() {
        return cureNumber;
    }

    public void setCureNumber(int cureNumber) {
        this.cureNumber = cureNumber;
    }

    public double getCureRate() {
        return cureRate;
    }

    public void setCureRate(double cureRate) {
        this.cureRate = cureRate;
    }

    public int getSumTouchNumber() {
        return sumTouchNumber;
    }

    public void setSumTouchNumber(int sumTouchNumber) {
        this.sumTouchNumber = sumTouchNumber;
    }

    public int getCurrentTouchNumber() {
        return currentTouchNumber;
    }

    public void setCurrentTouchNumber(int currentTouchNumber) {
        this.currentTouchNumber = currentTouchNumber;
    }

    public List getDates() {
        return dates;
    }

    public void setDates(List dates) {
        this.dates = dates;
    }

    public List getPatientNums() {
        return patientNums;
    }

    public void setPatientNums(List patientNums) {
        this.patientNums = patientNums;
    }

    public List getCureNums() {
        return cureNums;
    }

    public void setCureNums(List cureNums) {
        this.cureNums = cureNums;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
