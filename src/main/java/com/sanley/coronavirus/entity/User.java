package com.sanley.coronavirus.entity;/*
Created by shkstart on 2020/3/15.
*/

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.util.List;

@NoArgsConstructor
@Data
@Accessors(chain=true)
public class User {
    private int id;
    private String name;
    private String username;
    private String password;
    private List<Authentication> authenticationList;
    private BigInteger phone;
    private String unit;

    public boolean isAdmin(){
        boolean flag=false;
        for (Authentication authentication:authenticationList){
            if ("Admin".equals(authentication.getName())){
                flag=true;
            }

        }
        return flag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Authentication> getAuthenticationList() {
        return authenticationList;
    }

    public void setAuthenticationList(List<Authentication> authenticationList) {
        this.authenticationList = authenticationList;
    }

    public BigInteger getPhone() {
        return phone;
    }

    public void setPhone(BigInteger phone) {
        this.phone = phone;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
